const express = require("express");
const app = express();
const fs = require("fs");

const port = 5000;

app.use(express.static("./public"));

let files = fs.readdirSync(__dirname + "/public/app/components/views");

app.get("/components", (req, res) => {
  res.send(files);
});
fs.watch(__dirname + "/public/app/components/views", { persistent: true }, (event, fileName) => {
  //console.log(event + ": " + fileName);
  files = fs.readdirSync(__dirname + "/public/app/components/views");
  //console.log(files);
});

app.listen(port, () => {
    console.log("App is listening on port " + port);
});
